Rails.application.routes.draw do
  apipie if Rails.env.development?

  namespace :api do
    resources :users, only: [] do
      collection do
        post 'sign_up', to: 'users/registrations#sign_up'
        post 'sign_in', to: 'users/registrations#sign_in'
        put '/', to: 'users/users#update'
        patch '/', to: 'users/users#update'
        get 'profile', to: 'users/users#profile'

        resources :tasks, only: [:update, :create, :show, :destroy, :index] do
          member do
            post :change_status
          end
        end
        resources :missions
      end
    end
  end
end
