Apipie.configure do |config|
  config.app_name                = "Backend"
  config.api_base_url            = "/api"
  config.doc_base_url            = "/apipie"
  config.translate               = false
  config.validate                = false
  config.reload_controllers      = true
  # where is your API defined?
  config.api_controllers_matcher = File.join(Rails.root, "app", "controllers", "**","*.rb")
end
