class AddMissionIdToTasks < ActiveRecord::Migration[6.0]
  def change
    add_reference :tasks, :mission

    drop_table :missions_tasks
  end
end
