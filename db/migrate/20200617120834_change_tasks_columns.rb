class ChangeTasksColumns < ActiveRecord::Migration[6.0]
  def change
    rename_column :tasks, :date, :start_date
    add_column :tasks, :start_time, :string
    add_column :tasks, :end_time, :string
    add_column :tasks, :end_repetition_date, :datetime
  end
end
