class CreateMissions < ActiveRecord::Migration[6.0]
  def change
    create_table :missions do |t|
      t.string :title
      t.string :process
      t.string :process_scope
      t.boolean :without_deadline
      t.datetime :start_date
      t.datetime :end_date
      t.belongs_to :user

      t.timestamps
    end
  end
end
