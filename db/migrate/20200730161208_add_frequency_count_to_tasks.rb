class AddFrequencyCountToTasks < ActiveRecord::Migration[6.0]
  def change
    add_column :tasks, :frequency_count, :integer
  end
end
