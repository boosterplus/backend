class RenameTaskColumns < ActiveRecord::Migration[6.0]
  def change
    rename_column :tasks, :action, :process
  end
end
