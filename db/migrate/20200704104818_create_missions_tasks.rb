class CreateMissionsTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :missions_tasks do |t|
      t.belongs_to :mission
      t.belongs_to :task
      t.timestamps
    end
  end
end
