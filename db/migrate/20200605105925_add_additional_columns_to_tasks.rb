class AddAdditionalColumnsToTasks < ActiveRecord::Migration[6.0]
  def change
    add_column :tasks, :frequency, :string
    add_column :tasks, :process_scope, :string
    add_column :tasks, :duration, :integer  
    add_column :tasks, :period, :string
    add_column :tasks, :status, :string
  end
end
