# Booster+ Backend

## Requirements

- Ruby 2.7.1
- Rails 6
- PostgreSQL 12

## Setup

__To run:__

1.Create database.yml

```sh
cp config/database.example.yml config/database.yml
```

2.Install dependencies, create db, run migrations and run server

```sh
bundle install
rake db:setup
rake db:migrate
rails s
```
