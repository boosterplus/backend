class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :trackable, :validatable

  #mount_uploader :avatar, AvatarUploader

  has_secure_token :api_token

  #validates_presence_of :name

  has_many :tasks
  has_many :missions
end
