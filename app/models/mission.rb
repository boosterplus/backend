class Mission < ApplicationRecord
  extend Enumerize

  has_many :missions_tasks, dependent: :destroy, class_name: 'MissionsTask'
  has_many :tasks

  belongs_to :user

  enumerize :process, in: %w(level_up manage impact), i18n_scope: 'process'
  enumerize :process_scope, in: %w(lifetime health energy abilities finance connection information technology), i18n_scope: 'process_scope'

  scope :by_year, -> (year) { where("extract(year from start_date) = ?", year) }
  scope :by_month, -> (month) { where("extract(month from start_date) = ?", month) }
  scope :by_day, -> (day) { where("extract(day from start_date) = ?", day) }

  validates_presence_of :title, :process
end
