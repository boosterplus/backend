class Task < ApplicationRecord
  extend Enumerize

  has_many :repetition_tasks, class_name: "Task",
                          foreign_key: "parent_id",
                          dependent: :destroy

  belongs_to :parent, class_name: "Task", optional: true
  belongs_to :mission, optional: true
  belongs_to :user

  enumerize :process, in: %w(level_up manage impact), i18n_scope: 'process'
  enumerize :process_scope, in: %w(lifetime health energy abilities finance connection information technology), i18n_scope: 'process_scope'
  enumerize :frequency, in: %w(every_day every_week every_month) #, i18n_scope: 'frequency'

  scope :by_year, -> (year) { where("extract(year from start_date) = ?", year) }
  scope :by_month, -> (month) { where("extract(month from start_date) = ?", month) }
  scope :by_day, -> (day) { where("extract(day from start_date) = ?", day) }

  validates_presence_of :title, :process, :start_date

  def full_tree
    return [self, self&.repetition_tasks&.to_a].flatten unless self.has_parent? # if we have a parent task

    return [self.parent, self.parent.repetition_tasks.to_a].flatten # if we have a children task
  end

  def has_children?
    self.repetition_tasks.any?
  end

  def has_parent?
    self.parent_id.present?
  end

  def is_parent?
    self.has_children? && !self.has_parent?
  end
end
