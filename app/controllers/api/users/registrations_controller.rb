class Api::Users::RegistrationsController < ApiController
  api :POST, '/users/sign_up', 'Sign up a user'
  param :_, Hash, 'It is just object/json without name' do
    param :name, String, desc: 'Name of the user'
    param :email, String, required: true, desc: 'Email of the user'
    param :password, String, required: true, desc: 'User password'
  end
  returns code: 200 do
    property :user, Hash, 'User object with api_token in it'
  end

  def sign_up
    @user = User.new(sign_up_params)

    if @user.save
      render json: UserSerializer.new(@user).serializable_hash
    else
      render json: { errors: @user.errors.full_messages }, status: :unprocessable_entity
    end
  end

  api :POST, '/users/sign_in', 'Sign in a user'
  param :_, Hash, 'It is just object/json without name' do
    param :email, String, required: true, desc: 'Email of the user'
    param :password, String, required: true, desc: 'User password'
  end
  returns code: 200 do
    property :user, Hash, 'User object with api_token in it'
  end

  def sign_in
    @user = User.find_by(email: sign_in_params[:email].downcase)

    render json: { errors: I18n.t('errors.messages.user.email_not_found') }, status: 404 and return if @user.nil?

    if @user.valid_password?(sign_in_params[:password])
      render json: UserSerializer.new(@user).serializable_hash
    else
      render json: { errors: I18n.t('errors.messages.user.wrong_password') }, status: 401
    end
  end

  private

  def sign_up_params
    params.permit(
      :email,
      :password,
      :name
    )
  end

  def sign_in_params
    params.permit(
      :email,
      :password
    )
  end
end
