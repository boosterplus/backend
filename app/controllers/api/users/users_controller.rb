class Api::Users::UsersController < ApiController
  before_action :check_current_user!
  before_action :set_user, only: [:show]

  api :GET, '/users/profile', 'Get User by api_token'
  param :api_token, String, required: true, desc: "API Token in header"
  def profile
    render json: UserSerializer.new(@current_user, params: { full_state: true }).serializable_hash
  end

  def show
    render json: UserSerializer.new(@user).serializable_hash
  end

  api :PUT, '/users', 'Update a user by api_token'
  param :api_token, String, required: true, desc: "API Token in header"
  param :_, Hash, 'It is just object/json without name' do
    param :name, String, desc: 'Name of the user'
    param :password, String, desc: 'User password'
  end
  def update
    if @current_user.update(update_params)
      render json: UserSerializer.new(@current_user).serializable_hash
    else
      render json: { errors: @current_user.errors.full_messages }, status: :unprocessable_entity
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def update_params
    params.permit(
      :name,
      :password
    )
  end
end
