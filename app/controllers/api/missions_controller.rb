class Api::MissionsController < ApiController
  before_action :check_current_user!
  before_action :set_mission, only: [:update, :destroy, :show]

  api :GET, '/users/missions', 'Get missions filtered by year/month/day'
  param :api_token, String, required: true, desc: "API Token in header"
  param :year, String
  param :month, String
  param :day, String
  def index
    @missions = Missions::GetMissionsByDate.new(
      year: params[:year],
      month: params[:month],
      day: params[:day],
      user_id: @current_user.id).get_mission

    render json: MissionSerializer.new(@missions, options: { is_collection: true }).serializable_hash
  end

  api :POST, '/users/missions', 'Create a mission for certain user'
  param :api_token, String, required: true, desc: "API Token in header"
  param :mission, Hash, required: true do
    param :title, String, require: true
    param :process, %w(level_up manage impact), require: true
    param :process_scope, %w(lifetime health energy abilities finance connection information technology), require: true
    param :without_deadline, [true, false]
    param :start_date, DateTime
    param :end_date, DateTime
    param :task_ids, Array, of: Integer
  end

  def create
    service = Missions::CreateMission.new(
      mission_params.merge(user_id: @current_user.id).to_h
    )

    @created_mission = service.create

    render json: { errors: service.errors }, status: :unprocessable_entity and return unless service.errors.empty?

    render json: MissionSerializer.new(@created_mission, options: { is_collection: true }).serializable_hash
  end

  api :PUT, '/users/missions/:id', 'Update a mission for certain user'
  param :api_token, String, required: true, desc: "API Token in header"
  param :id, Integer, required: true, desc: 'Task ID'
  param :mission, Hash, required: true do
    param :title, String
    param :process, %w(level_up manage impact)
    param :process_scope, %w(lifetime health energy abiliites finance connection information technology)
    param :without_deadline, [true, false]
    param :start_date, DateTime
    param :end_date, DateTime
    param :task_ids, Array, of: Integer
  end
  def update
    service = Missions::UpdateMission.new(
      mission_params.merge(user_id: @current_user.id, mission_id: @mission.id).to_h
    )

    @updated_mission = service.update

    render json: { errors: service.errors }, status: :unprocessable_entity and return unless service.errors.empty?

    render json: MissionSerializer.new(@updated_mission, options: {is_collection: true}).serializable_hash
  end

  api :DELETE, '/users/missions/:id', 'Delete a mission for certain user'
  param :api_token, String, required: true, desc: "API Token in header"
  param :id, Integer, required: true, desc: 'Mission ID'
  def destroy
    service = Missions::DestroyMission.new(@mission.id)

    @destroyed_mission = service.destroy

    render json: { errors: service.errors }, status: :unprocessable_entity and return unless service.errors.empty?

    render json: MissionSerializer.new([@destroyed_mission], options: { is_collection: true }).serializable_hash, status: :ok
  end

  api :GET, '/users/missions/:id', 'Get a mission by ID'
  param :api_token, String, required: true, desc: "API Token in header"
  param :id, Integer, required: true, desc: 'Mission ID'
  def show
    render json: MissionSerializer.new(@mission).serializable_hash
  end

  private
  def set_mission
    @mission = @current_user.missions.find(params[:id])
  end

  def mission_params
    params.require(:mission).permit(
      :title,
      :process,
      :process_scope,
      :without_deadline,
      :start_date,
      :end_date,
      :task_ids => []
    )
  end
end
