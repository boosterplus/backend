class Api::TasksController < ApiController
  before_action :check_current_user!
  before_action :set_task, only: [:update, :destroy, :show, :change_status]

  api :GET, '/users/tasks', 'Get tasks filtered by year/month/day'
  param :api_token, String, required: true, desc: "API Token in header"
  param :year, String
  param :month, String
  param :day, String
  def index
    @tasks = Tasks::GetTasksByDate.new(
      year: params[:year],
      month: params[:month],
      day: params[:day],
      user_id: @current_user.id).get_task

    render json: TaskSerializer.new(@tasks, options: { is_collection: true }).serializable_hash
  end

  api :POST, '/users/tasks', 'Create a task for certain user'
  param :api_token, String, required: true, desc: "API Token in header"
  param :task, Hash, required: true do
    param :title, String
    param :process, %w(level_up manage impact), require: true
    param :process_scope, %w(lifetime health energy abilities finance connection information technology)
    param :description, String
    param :duration, Integer
    param :start_date, DateTime
    param :start_time, String
    param :mission_id, Integer
    param :frequency, ['every_day', 'every_week', 'every_month']
    param :frequency_count, Integer
    param :end_repetition_date, DateTime
  end
  def create
    service = Tasks::CreateTask.new(
      task_params.merge(user_id: @current_user.id).to_h
    )

    @created_tasks = service.create

    render json: { errors: service.errors }, status: :unprocessable_entity and return unless service.errors.empty?

    render json: TaskSerializer.new(@created_tasks, options: { is_collection: true }).serializable_hash
  end

  api :PUT, '/users/tasks/:id', 'Update a task for certain user'
  param :api_token, String, required: true, desc: "API Token in header"
  param :id, Integer, required: true, desc: 'Task ID'
  param :task, Hash, required: true do
    param :title, String, require: true
    param :process, %w(level_up manage impact)
    param :process_scope, %w(lifetime health energy abilities finance connection information technology)
    param :status, String
    param :description, String
    param :duration, Integer
    param :start_date, DateTime, require: true
    param :start_time, String
    param :mission_id, Integer
    param :frequency, ['every_day', 'every_week', 'every_month']
    param :frequency_count, Integer
    param :end_repetition_date, DateTime
  end
  def update
    service = Tasks::UpdateTask.new(
      task_params.merge(user_id: @current_user.id, task_id: @task.id).to_h
    )

    @updated_task = service.update
    render json: { errors: service.errors }, status: :unprocessable_entity and return unless service.errors.empty?

    render json: TaskSerializer.new(@updated_task, options: {is_collection: true}).serializable_hash
  end

  api :DELETE, '/users/tasks/:id', 'Delete a task for certain user'
  param :api_token, String, required: true, desc: "API Token in header"
  param :id, Integer, required: true, desc: 'Task ID'
  param :delete_all, [true, false]
  def destroy
    service = Tasks::DestroyTask.new(
      delete_all_params.merge(task_id: @task.id).to_h
    )

    @destroyed_task = service.destroy


    render json: { errors: service.errors }, status: :unprocessable_entity and return unless service.errors.empty?

    render json: TaskSerializer.new([@destroyed_task], options: { is_collection: true }).serializable_hash, status: :ok
  end

  api :GET, '/users/tasks/:id', 'Get a task by ID'
  param :api_token, String, required: true, desc: "API Token in header"
  param :id, Integer, required: true, desc: 'Task ID'
  def show
    render json: TaskSerializer.new(@task).serializable_hash
  end

  api :POST, '/users/tasks/:id/change_status', 'Change status for task by id'
  param :api_token, String, required: true, desc: "API Token in header"
  param :id, Integer, required: true, desc: 'Task ID'
  param :_, Hash, 'It is just object/json without name' do
    param :status, String, required: true, desc: 'Status of a Task'
  end
  def change_status
    if @task.update(change_status_params)
      render json: TaskSerializer.new([@task], options: {is_collection: true}).serializable_hash
    else
      render json: { errors: @task.errors.full_messages }, status: :unprocessable_entity
    end
  end

  private

  def delete_all_params
    params.permit(
      :id,
      :delete_all
    )
  end

  def set_task
    @task = @current_user.tasks.find(params[:id])
  end

  def task_params
    params.require(:task).permit(
      :title,
      :process,
      :process_scope,
      :status,
      :description,
      :duration,
      :start_date,
      :start_time,
      :mission_id,
      :frequency,
      :frequency_count,
      :end_repetition_date
    )
  end

  def change_status_params
    params.permit(
      :status
    )
  end
end
