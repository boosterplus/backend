class ApiController < ApplicationController
  before_action :authenticate_by_api_token!

  private

  def authenticate_by_api_token!
    @current_user = User.find_by_api_token(request.headers['Authorization']&.split(' ')&.second)
  end

  def check_current_user!
    render json: { errors: I18n.t('errors.messages.user.need_to_authenticate') }, status: 401 if @current_user.nil?
  end
end
