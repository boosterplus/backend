class MissionSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title,
    :process, :process_scope,
    :without_deadline,
    :start_date, :end_date,
    :created_at, :updated_at,
    :tasks

  # It's for the enumerize with I18n, really easy, just change locale and go
  # It will pick up locale and get translation from the ru/en yaml
  attribute :process do |object|
    object&.process&.text
  end

  attribute :process_scope do |object|
    object&.process_scope&.text
  end

  attribute :tasks do |object|
    TaskSerializer.new(object&.tasks, options: { is_collection: true }).serializable_hash
  end
end
