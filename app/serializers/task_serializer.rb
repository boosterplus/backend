class TaskSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title,
    :process, :process_scope,
    :description, :status,
    :duration, :start_date,
    :start_time, :end_time,
    :mission_id, :parent_id,
    :frequency, :frequency_count,
    :end_repetition_date,
    :created_at, :updated_at

  # It's for the enumerize with I18n, really easy, just change locale and go
  # It will pick up locale and get translation from the ru/en yaml
  attribute :process do |object|
    object&.process&.text
  end

  attribute :process_scope do |object|
    object&.process_scope&.text
  end
end
