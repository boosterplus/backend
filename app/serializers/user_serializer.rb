class UserSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name,
    :email, :api_token,
    :created_at, :updated_at

  attribute :missions, if: Proc.new { |_, params|
    params && params[:full_state] == true
  } do |object|
    MissionSerializer.new(object.missions,
      options: { is_collection: true }).serializable_hash
  end

  attribute :tasks, if: Proc.new { |_, params|
    params && params[:full_state] == true
  } do |object|
    TaskSerializer.new(object.tasks, options: { is_collection: true }).serializable_hash
  end
end
