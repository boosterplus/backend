module Missions
  class DestroyMission
    attr_reader :errors

    def initialize(mission_id)
      @mission = Mission.find(mission_id)
      @errors = []
    end

    def destroy
      return @mission.destroy
    rescue StandardError => error
      @errors << error.message
    end
  end
end