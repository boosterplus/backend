module Missions
  class CreateMission
    attr_reader :errors

    CURRENT_YEAR = Time.zone.now.year
    CURRENT_MONTH = Time.zone.now.month
    CURRENT_DAY = Time.zone.now.day

    def initialize(
      params = {}
    )
      @title,
      @process,
      @process_scope,
      @without_deadline,
      @start_date,
      @end_date,
      @task_ids,
      @user_id = params.values_at(*%w(title process process_scope without_deadline start_date end_date task_ids user_id))

      @user = User.find(@user_id)
      @errors = []
    end

    def create
      # TODO: refactor this with rescue errors and transaction
      @process_scope = nil if @process == 'manage' or @process == 'impact'

      attributes = {
        title: @title,
        process: @process,
        process_scope: @process_scope,
        without_deadline: @without_deadline,
        start_date: @start_date,
        end_date: @end_date,
        task_ids: @task_ids,
        user: @user
      }

      @mission = Mission.new(attributes)
      @mission.save!
      return [@mission]

    rescue StandardError => error
      @errors << error.message
    end
  end
end