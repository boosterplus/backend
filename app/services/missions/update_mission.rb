module Missions
  class UpdateMission
    attr_reader :errors

    def initialize(
      params = {}
    )
      @title,
      @process,
      @process_scope,
      @without_deadline,
      @start_date,
      @end_date,
      @task_ids,
      @user_id,
      @mission_id = params.values_at(*%w(title process process_scope without_deadline start_date end_date task_ids user_id mission_id))
      @user = User.find(@user_id)
      @mission = Mission.find(@mission_id)
      @errors = []
    end

    def update
      # TODO: refactor this with rescue errors and transaction
      @process_scope = nil if @process == 'manage' or @process == 'impact'

      attributes = {
        title: @title,
        process: @process,
        process_scope: @process_scope,
        without_deadline: @without_deadline,
        start_date: @start_date,
        end_date: @end_date,
        task_ids: @task_ids,
        user: @user
      }

      @mission.assign_attributes(attributes.compact)

      @mission.save!
      return [@mission]
    rescue StandardError => error
      @errors << error.message
    end
  end
end