module Tasks
  class DestroyTask
    attr_reader :errors

    def initialize(params = {})
      @task_id,
      @delete_all = params.values_at(*%w(task_id delete_all))

      @task = Task.find(@task_id)
      @errors = []

      @delete_all ||= false # If delete_all is nil
    end

    def destroy
      # destroy single task
      return destroy_single_task unless @delete_all
      
      # destroy all linked tasks
      return destroy_all_tasks

    rescue StandardError => error
      @errors << error.message
    end

    def recreate_links
      if @task.is_parent?
        related = @task.repetition_tasks
        new_related_count = related.count > 1 ? related.count - 1 : related.count
        parent = related.first

        parent.update(parent_id: nil)

        related.order(created_at: :desc).limit(new_related_count).update_all(parent_id: parent.id)
      end
    end

    def destroy_single_task
      Task.transaction do
        return @task.delete.tap do
          recreate_links
        end unless !@task.has_children?
        
        @task.destroy!
        return [@task]
      end
    end

    def destroy_all_tasks
      # if task is parent, using dependent-destroy
      return @task.destroy unless !@task.is_parent?

      return @task.full_tree.map(&:delete)
    end
  end
end