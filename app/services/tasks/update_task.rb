module Tasks
  class UpdateTask
    attr_reader :errors

    def initialize(
      params = {}
    )
      @title,
      @process,
      @process_scope,
      @description,
      @status,
      @start_date,
      @start_time,
      @duration,
      @mission_id,
      @frequency,
      @frequency_count,
      @end_repetition_date,
      @user_id,
      @task_id = params.values_at(*%w(title process process_scope description status start_date start_time duration mission_id frequency frequency_count end_repetition_date user_id task_id))
      @end_time = nil
      @user = User.includes(:tasks).find(@user_id)
      @task = Task.includes(:repetition_tasks).find(@task_id)
      @errors = []
    end

    def update

      # TODO: refactor this with rescue errors and transaction

      attributes = {
        title: @title,
        process: @process,
        process_scope: @process_scope,
        description: @description,
        status: @status,
        duration: @duration,
        start_date: @start_date,
        start_time: get_start_time,
        end_time: calculate_end_time,
        mission_id: @mission_id,
        frequency: @frequency,
        frequency_count: @frequency_count,
        end_repetition_date: @end_repetition_date,
      }

      @start_date = @task.start_date if @start_date.nil?
      @start_time = @task.start_time if @start_time.nil?
      @end_time = @task.end_time if @end_time.nil?

      check_intersection(@start_date, @start_time, @end_time)

      @task.assign_attributes(attributes.compact)

      @task.save!
      return [@task] unless (@frequency && @end_repetition_date)

      @task.full_tree.sort_by(&:start_date)
    rescue StandardError => error
      @errors << error.message
    end

    def check_intersection(start, start_time, end_time)
      start_time, end_time = Time.parse(start_time), Time.parse(end_time)
      date = start.is_a?(String) ? DateTime.parse(start) : start
      year, month, day = date.year, date.month, date.day

      # First, get task for this year
      ##
      # Second, check if there are any tasks for this month
      # If any check the day and time
      # Here we are checking for the parent, not the repetition

      if @user.tasks.where.not(id: @task.id).by_year(year).by_month(month).any?
        scope = @user.tasks.where.not(id: @task.id).by_year(year).by_month(month).by_day(day)
        raise StandardError, I18n.t('errors.messages.task.time_intersects_with_another_task') if scope.any? {
          |t| (start_time >= Time.parse(t.start_time) &&  end_time <= Time.parse(t.end_time)) || (time_overlaps?(Time.parse(t.start_time), Time.parse(t.end_time), start_time, end_time))
        }

      end
    end

    def get_start_time
      return nil unless @start_date.present?

      @start_time = DateTime.parse(@start_date).strftime('%H:%M')
      @start_time
    end

    def calculate_end_time
      return nil unless @start_time.present?

      @end_time = (Time.zone.parse(@start_time) + @duration.to_i.minutes).strftime('%H:%M')
      @end_time
    end

    def time_overlaps?(first_start, first_end, second_start, second_end)
      first_start <= second_end && second_start < first_end
    end
  end
end