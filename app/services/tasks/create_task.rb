module Tasks
  class CreateTask
    attr_reader :errors

    CURRENT_YEAR = Time.zone.now.year
    CURRENT_MONTH = Time.zone.now.month
    CURRENT_DAY = Time.zone.now.day

    def initialize(
      params = {}
    )
      @title,
      @process,
      @process_scope,
      @description,
      @start_date,
      @start_time,
      @duration,
      @mission_id,
      @frequency,
      @frequency_count,
      @end_repetition_date,
      @user_id = params.values_at(*%w(title process process_scope description start_date start_time duration mission_id frequency frequency_count end_repetition_date user_id))
      @end_time = nil
      @user = User.includes(:tasks).find(@user_id)
      @errors = []
    end

    def create
      # TODO: refactor this with rescue errors and transaction

      attributes = {
        title: @title,
        process: @process,
        process_scope: @process_scope,
        description: @description,
        duration: @duration,
        start_date: @start_date,
        start_time: get_start_time,
        end_time: calculate_end_time,
        mission_id: @mission_id,
        frequency: @frequency,
        frequency_count: @frequency_count,
        end_repetition_date: @end_repetition_date,
        user: @user
      }

      check_intersection(@start_date, @start_time, @end_time)

      @task = Task.new(attributes)

      if @frequency && @end_repetition_date
        repetition_tasks = create_repetition_tasks
        return nil unless repetition_tasks.present?
        @task.save!
        repetition_tasks.each { |t| t.save! }
        return [@task] + repetition_tasks
      end

      @task.save!
      [@task]
    rescue StandardError => error
      @errors << error.message
    end

    def create_repetition_tasks
      case @frequency
      when 'every_day'
        repetition_interval(@frequency_count.day)
      when 'every_week'
        repetition_interval(@frequency_count.week)
      when 'every_month'
        repetition_interval(@frequency_count.month)
      end
    end

    def repetition_interval(step)
      start_date = DateTime.parse(@start_date)
      starting = start_date + step
      ending = DateTime.parse(@end_repetition_date)
      prev_task = @task

      raise StandardError, I18n.t('errors.messages.task.end_date_before_start_date') unless starting <= ending

      interval = []
      ti = 1

      while starting <= ending
        check_intersection(starting, @start_time, @end_time)

        new_task = Task.new(
          title: @title,
          process: @process,
          process_scope: @process_scope,
          description: @description,
          duration: @duration,
          start_date: starting,
          start_time: get_start_time,
          end_time: calculate_end_time,
          frequency: @frequency,
          end_repetition_date: @end_repetition_date,
          user: @user,
          parent: prev_task
        )
        interval << new_task

        ti += 1
        starting = start_date + ti * step
        prev_task = new_task
      end
      interval
    end

    def check_intersection(start, start_time, end_time)
      start_time, end_time = Time.parse(start_time), Time.parse(end_time)
      date = start.is_a?(String) ? DateTime.parse(start) : start
      year, month, day = date.year, date.month, date.day

      # First, get task for this year
      ##
      # Second, check if there are any tasks for this month
      # If any check the day and time
      # Here we are checking for the parent, not the repetition

      if @user.tasks.by_year(year).by_month(month).any?
        scope = @user.tasks.by_year(year).by_month(month).by_day(day)
        raise StandardError, I18n.t('errors.messages.task.time_intersects_with_another_task') if scope.any? {
          |t| (start_time >= Time.parse(t.start_time) &&  end_time <= Time.parse(t.end_time)) || (time_overlaps?(Time.parse(t.start_time), Time.parse(t.end_time), start_time, end_time))
        }

      end
    end

    def get_start_time
      @start_time = DateTime.parse(@start_date).strftime('%H:%M')
      @start_time
    end

    def calculate_end_time
      @end_time = (Time.zone.parse(@start_time) + @duration.to_i.minutes).strftime('%H:%M')
      @end_time
    end

    def time_overlaps?(first_start, first_end, second_start, second_end)
      first_start <= second_end && second_start < first_end
    end
  end
end