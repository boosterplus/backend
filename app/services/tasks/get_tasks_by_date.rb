module Tasks
  class GetTasksByDate
    attr_reader :errors
    
    CURRENT_YEAR = Time.zone.now.year
    CURRENT_MONTH = Time.zone.now.month
    CURRENT_DAY = Time.zone.now.day

    def initialize(year: nil, month: nil, day: nil, user_id: nil)
      @year = year
      @month = month
      @day = day
      @user = User.find(user_id)
    end

    def get_task
      return @user.tasks if [@year, @month, @day].all?(nil)
      
      return Task.by_year(@year).by_month(@month).by_day(@day) if [@year, @month, @day].all? {|i| !i.nil?}
      
      case
      when @year
        return Task.by_year(@year)
      when @month
        return Task.by_year(CURRENT_YEAR).by_month(@month)
      when @day
        return Task.by_year(CURRENT_YEAR).by_month(CURRENT_MONTH).by_day(@day)
      end
    end
  end
end